import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../controllers/UserCtrl.dart';
import '../widgets/ChampsSaisie.dart';
import '../widgets/Chargement.dart';
import 'FidelesPage.dart';

class AddFideles extends StatefulWidget {
  const AddFideles({Key? key}) : super(key: key);

  @override
  State<AddFideles> createState() => _AddFidelesState();
}

class _AddFidelesState extends State<AddFideles> {
  Color couleurFond = Colors.white;
  bool isVisible = false;
  var formKey = GlobalKey<FormState>();
  var fidelname = TextEditingController();
  var fidelsecondN = TextEditingController();
  var fidelage = TextEditingController();
  var fidelimage = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: couleurFond,
      body: Stack(
        children: [_body(context), Chargement(isVisible)],
      ),
    );
  }

  Widget _body(BuildContext context) {
    return Form(
      key: formKey,
      child: Center(
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.symmetric(horizontal: 40),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                _iconApp(),
                SizedBox(
                  height: 40,
                ),
                Text("Ajout Fidele", style: TextStyle(fontSize: 20)),
                SizedBox(
                  height: 20,
                ),
                ChampSaisie(ctrl: fidelname, label: "Nom du fidele", required: true,prefixIcon: Icons.person),
                SizedBox(
                  height: 20,
                ),
                ChampSaisie(ctrl: fidelsecondN, label: "Prenom du fidele", required: true,prefixIcon: Icons.person),
                SizedBox(
                  height: 20,
                ),
                ChampSaisie(ctrl: fidelage, label: "Age du fidele", required: true,prefixIcon: Icons.apps_outage),
                SizedBox(
                  height: 20,
                ),
                SizedBox(
                  height: 20,
                ),
                //_textError(),
                _buttonWidget(context),
                SizedBox(
                  height: 50,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _iconApp() {
    return IconButton(onPressed: (){

    }, color: Colors.orange,icon: Icon(Icons.photo_library_outlined,size: 50,));
  }

  Widget _buttonWidget(BuildContext ctx) {
    return Container(
      width: 500,
      height: 50,
      child: ElevatedButton(
        onPressed: () async {

        },
        child: Text("Ajouter un fidele"),
        style: ElevatedButton.styleFrom(
            primary: Colors.orange,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16))),
      ),
    );
  }

  //Widget _textError() {
    //return Text(errorMsg, style: TextStyle(color: Colors.red, fontSize: 16));
  //}
}
